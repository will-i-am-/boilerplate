/**
 * Gulp file
 * 
 */
var gulp 			= require('gulp'),
	sass 			= require('gulp-sass'),
	plumber 		= require('gulp-plumber'),
	stripdebug 		= require('gulp-strip-debug'),
	cachebust 		= require('gulp-css-urlversion'),
	imagemin 		= require('gulp-imagemin'),
	spritesmith 	= require('gulp.spritesmith'),
	banner 			= require('gulp-banner'),
	csso 			= require('gulp-csso'),
	uglify 			= require('gulp-uglify'),
	concat 			= require('gulp-concat'),
	autoprefix  	= require('gulp-autoprefixer'),
	bowerfiles  	= require('main-bower-files'),
    filter      	= require('gulp-filter'),
	pkg 			= require('./package.json'),
	sourcemaps 		= require('gulp-sourcemaps'),
	buffer  		= require('vinyl-buffer'),
	rename   		= require('gulp-rename'),
	gutil 			= require('gulp-util'),
	zip 			= require('gulp-zip'),
	clean 			= require('gulp-clean'),
	merge 			= require('merge-stream'),
	jasmineBrowser 	= require('gulp-jasmine-browser'),
	connect 		= require('gulp-connect'),
	scsslint 		= require('gulp-scss-lint'),
	jshint 			= require('gulp-jshint');

/**
 * Config Object:
 * Contains all the paths needed for the gulp tasks to run
 * The file structure has been thought about carefully
 * So make changes only if you have too.
 * @type {Object}
 */
var config = {
	dest: "./",
	sassPath: "./css/src/",
	jsSrcPath: "./scripts/src/",
	jsSpecPath: "./scripts/spec/",
	cssPath:  "./css",
	jsPath: "./scripts",
	name: pkg.name,
	devPath: "./dev/"
}

/**
 * Scripts array:
 * Add all your JS files here you wish to be concatenated into one file.
 * The gulp tasks will respect the order in which they are put into the array.
 * @type {Array}
 */
var scripts = [];

var comment = 
	'/*\n' +
    ' * <%= pkg.name %> <%= pkg.version %>\n' +
    ' * Updated: <%= new Date().toString() %>\n' +
    ' *\n' +
    ' * Copyright <%= new Date().getFullYear() %>, <%= pkg.author %>\n' +
    '*/\n\n';

/**
 * sassFunction:
 * Used across two Gulp tasks,
 * One of which just builds the SASS,
 * The other ensures that the sprite task runs first for the cachebusting to work
 */
var sassFunction = function(){
	return gulp.src(config.sassPath + 'main.scss')
			.pipe(plumber())
			.pipe(sourcemaps.init())
	        .pipe(sass.sync()) // Have to use sass.sync - See Issue (https://github.com/dlmanning/gulp-sass/issues/90)  	        
	        .pipe(cachebust({baseDir: './'}))	
	        .pipe(autoprefix({
	                browsers: ['last 2 versions'],
	                cascade: false
	            }))	  	        
	        .pipe(banner(comment, {
            	pkg: pkg
        	}))             	 
        	.pipe(rename(config.name +'.css'))       
        	.pipe(sourcemaps.write('.'))     	        	   	
	        .pipe(gulp.dest(config.cssPath));
}

/**
 * Task: Sprite
 * 
 * Takes all images from the sprite folder and creates a
 * Single image from them and places the image in the root image folder.
 * It also creates as SCSS file for use within SASS
 */
gulp.task('sprite', function(done){
	var spriteData = gulp.src('./images/src/sprite/*.png')
		.pipe(spritesmith({
		    imgName: 'sprite.png',
		    cssName: 'sprite.scss',
		    imgPath: '/images/sprite.png',
		    padding: 5,
		    algorithm: 'top-down'
		  }));

  	spriteData.img
  		.pipe(buffer())
	  	.pipe(imagemin())
	  	.pipe(gulp.dest('./images'))
	  	.on('end', function () { done(); });

	spriteData.css
		.pipe(gulp.dest('./css/src/base'));

});

/**
 * Task: Imagemin
 * Compresses images to get the filesize as small as possible
 */
gulp.task('imagemin', function(){
	return gulp.src('./images/*.{png, gif, jpg, jpeg}')
		.pipe(imagemin())
		.pipe(gulp.dest('./images/'))
});

/**
 * Task: Sass-build
 * Runs the sprite task first in order for the cachebusting to work,
 * The runs the standard SASS task
 */
gulp.task('sass-build', ['sprite'], function(){
	return sassFunction();
});

/**
 * Task: Sass
 * Runs and compiles the SASS to CSS
 * This should be the default task used for SASS when a Sprite is not needed
 */
gulp.task('sass', function(){
	return sassFunction();
});

/**
 * Task: Sass-Production
 * Runs the Sprite task first
 * Minifies CSS on completion, for use in production
 */
gulp.task('sass-production', ['sprite'], function(){
	return gulp.src(config.sassPath + 'main.scss')
			.pipe(plumber())
	        .pipe(sass.sync()) // Have to use sass.sync - See Issue (https://github.com/dlmanning/gulp-sass/issues/90)  	        
	        .pipe(cachebust({baseDir: './'}))	
	        .pipe(autoprefix({
	                browsers: ['last 2 versions'],
	                cascade: false
	            }))                   
	        .pipe(csso())
	        .pipe(banner(comment, {
            	pkg: pkg
        	}))        	
        	.pipe(rename(config.name +'.min.css'))        	
        	.pipe(plumber.stop())        	
	        .pipe(gulp.dest(config.cssPath));
})

/**
 * Task: Js
 * Takes in the array of Script files
 * Combines and minifies all JS scripts into a single file
 * Strips any console.logs
 */
gulp.task('js', function(){
	return gulp.src(scripts)
			.pipe(plumber())
			.pipe(sourcemaps.init())
			.pipe(stripdebug())
			.pipe(concat('<%= pkg.name %>.min.js'))
			.pipe(uglify({
				mangle: false
			}))
			.pipe(banner(comment, {
	            	pkg: pkg
	        	}))
			.pipe(sourcemaps.write())
			.pipe(plumber.stop())
			.pipe(gulp.dest(config.jsPath));
});

/**
 * Task: Jasmine-Phantom
 * If there are any Unit tests for JS, then this task will run them
 */
gulp.task('jasmine-phantom', function() {
  return gulp.src([config.jsSrcPath + '*.js', config.jsSpecPath + '*_spec.js'])
    .pipe(jasmineBrowser.specRunner({console: true}))
    .pipe(jasmineBrowser.headless());
});

/**
 * Task: Bower
 * Find all main files within the bower_components folder and move them into the css/scripts folders
 * Relies on bower install running prior to running this task
 */
gulp.task('bower', function(){

    var filterByExtension = function(extension){  
            return filter(function(file){
                return file.path.match(new RegExp('.*.' + extension + '$'));
            },  {restore: true});
        };

    var mainFiles = bowerfiles({ env : "production"});

    if(!mainFiles.length){
        // No main files found. Skipping....
        return;
    }

    var jsFilter = filterByExtension('js');

    return gulp.src(mainFiles)
        .pipe(jsFilter)
        .pipe(gulp.dest(config.dest + 'scripts/vendor/'))
        .pipe(jsFilter.restore)
        .pipe(filterByExtension('css'))
        .pipe(gulp.dest(config.dest + 'css/vendor/'))
});

/**
 * Task: Connect
 * Creates a local server for running any dev HTML prior to a dev site being set-up
 * root: should point to the dev folder
 */
gulp.task('connect', function() {
  connect.server({
    root: './',
    livereload: true
  });
});

/**
 * Task: Scss-Lint
 * Runs a check on your scss files to check they conform to standards
 */
gulp.task('scss-lint', function() {
  return gulp.src(config.sassPath + '**/*.scss')
    .pipe(scsslint({'config': 'lint.yml'}));
});

/**
 * Task: Watch
 * 
 */
gulp.task('watch', function(){
	gulp.watch(config.sassPath + '**/*.scss', ['sass']);
	gulp.watch(config.jsPath + '/src/*.js', ['js']);
});

/**
 * Task: JSlint
 * Runs a check on your js files to check they conform to standards
 */
gulp.task('jslint', function() {
  return gulp.src(config.jsPath + '/src/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

/**
 * User Writes there own list based on tasks they need for development
 */
gulp.task('development', []);

/**
 * User Writes there own list based on tasks they need for production
 */
gulp.task('production', []);

/**
 * Default Task
 * Checks to see what enviroment it is in and then runs the task specific to it.
 * Enviroment can be changed by running this command:
 * SET NODE_ENV=production
 * or
 * SET NODE_ENV=development
 */
gulp.task('default', [process.env.NODE_ENV === 'production' ? 'production' : 'development']);